use std::{
    fs::{self, File},
    path::PathBuf,
};

use clap::Parser;
use color_eyre::{eyre::eyre, Result};
use log::{trace, warn};
use serde::{Deserialize, Serialize};

/// Saleae logic LIN analyzer CSV output transformer
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// CSV input from the Saleae LIN analyzer output
    #[arg(short, long)]
    input_file: PathBuf,

    /// CSV output filename
    #[arg(short, long)]
    output_file: Option<PathBuf>,

    /// Don't process checksums
    #[arg(short, long, default_value_t = false)]
    skip_checksums: bool,
}

#[derive(Debug, Clone, Deserialize)]
pub struct CSVRow {
    #[serde(rename = "type")]
    event_type: EventTypes,
    // name: String,
    start_time: f32,
    duration: f32,
    data: Option<u8>,
    protected_id: Option<u8>,
    // index: i8,
}

#[derive(Debug, Clone, Deserialize, PartialEq)]
pub enum EventTypes {
    #[serde(rename = "header_break")]
    HeaderBreak,
    #[serde(rename = "header_sync")]
    HeaderSync,
    #[serde(rename = "header_pid")]
    HeaderPid,
    #[serde(rename = "data")]
    Data,
    #[serde(rename = "checksum")]
    Checksum,
    #[serde(rename = "data_or_checksum")]
    DataOrChecksum,
}

#[derive(Debug, Clone, Serialize, PartialEq)]
pub struct Packet {
    timestamp: f32,
    duration: f32,
    valid_structure: bool,
    valid_checksum: bool,
    checksum: u8,
    pid: u8,
    data: Vec<u8>,
}

impl Default for Packet {
    fn default() -> Packet {
        Packet {
            timestamp: 0.0,
            duration: 0.0,
            pid: 0,
            data: vec![],
            valid_structure: false,
            valid_checksum: false,
            checksum: 0,
        }
    }
}

#[derive(PartialEq)]
pub enum PacketStateMachine {
    SyncWaiting,
    GettingPid(Packet),
    CollectingData(Packet),
    Terminal(Packet),
}

impl PacketStateMachine {
    /*
                All events that aren't labelled are
                implicit transitions to the Terminal
                state with an invalid packet containing
                any fields or data that have been
                received so far.  If read_packet isn't
                called after every transition, packets
                might be missed because you can't
                transition out of Finalize without it.


        ┌─────────┐  Sync    ┌─────┐
        │Wait for ├──────────► Get │
     ┌──►  Sync   │          │ PID │
    *│  └┬───▲────┘          └┬───┬┘
     └───┘   │Read Packet     │   │ Data
             │                │   │
        ┌────┴────┐  Break    │ ┌─▼───────┐
        │Finalize ◄───────────┘ │ Collect ◄──┐
     ┌──►         ◄─────────────┤   Data  │  │ Data or
    *│  └┬────────┘  Header     └────────┬┘  │ Data-or-Checksum
     └───┘           Break               └───┘
                 */
    pub fn transition(self, row: CSVRow) -> Self {
        match self {
            PacketStateMachine::SyncWaiting => match row.event_type {
                EventTypes::HeaderSync => Self::GettingPid(Packet {
                    timestamp: row.start_time,
                    duration: row.duration,
                    pid: 0,
                    data: vec![],
                    valid_structure: false,
                    valid_checksum: true,
                    checksum: 0,
                }),
                _ => self,
            },

            PacketStateMachine::GettingPid(mut packet) => match row.event_type {
                EventTypes::HeaderPid => {
                    if let Some(pid) = row.protected_id {
                        packet.pid = pid;
                        packet.duration += row.duration;
                        Self::CollectingData(packet)
                    } else {
                        Self::Terminal(Self::invalid_packet(row.start_time))
                    }
                }
                _ => Self::Terminal(Self::invalid_packet(row.start_time)),
            },

            PacketStateMachine::CollectingData(mut packet) => {
                packet.duration += row.duration;

                if let Some(data_byte) = row.data {
                    match row.event_type {
                        EventTypes::Data | EventTypes::DataOrChecksum => {
                            packet.data.push(data_byte);
                            PacketStateMachine::CollectingData(packet)
                        }
                        _ => {
                            packet.data.push(data_byte);
                            PacketStateMachine::Terminal(packet)
                        }
                    }
                } else {
                    if row.event_type == EventTypes::HeaderBreak {
                        packet.valid_structure = true;
                        PacketStateMachine::Terminal(packet)
                    } else {
                        PacketStateMachine::Terminal(packet)
                    }
                }
            }

            PacketStateMachine::Terminal(_) => self,
        }
    }

    pub fn read_packet(self, skip_checksums: bool) -> (Self, Option<Packet>) {
        if let Self::Terminal(mut packet) = self {
            if skip_checksums == false && packet.valid_structure {
                // Move data out of the packet struct
                let mut data = packet.data;
                // If the data vector has at least one byte, try to checksum it.
                if let Some(checksum) = data.pop() {
                    packet.checksum = checksum;

                    // Compute the checksum from packet data
                    let mut computed_checksum: u16 =
                        data.iter().fold(0, |acc, byte| acc + *byte as u16);
                    // I don't like this method of computing the checksum,
                    // but it's the only way that solves for 100% of the packets.
                    while computed_checksum > 255 {
                        computed_checksum -= 255
                    }
                    let computed_checksum = computed_checksum as u8 ^ 0xFF;

                    if checksum as u8 != computed_checksum {
                        warn!(
                            "Got a packet with a bad checksum {} != {} on packet: \n {}: {:?}",
                            checksum as u8, computed_checksum, packet.timestamp, &data
                        );
                        packet.valid_checksum = false;
                    }
                }
                // Move data back into packet
                packet.data = data;
            }

            return (PacketStateMachine::SyncWaiting, Some(packet));
        }
        (self, None)
    }

    fn invalid_packet(time: f32) -> Packet {
        let mut packet = Packet::default();
        packet.timestamp = time;

        packet
    }
}

fn main() -> Result<()> {
    pretty_env_logger::init();
    color_eyre::install()?;

    trace!("Parsing arguments");
    let args = Args::parse();

    // Read and deserialize the file
    let contents = fs::read_to_string(args.input_file)?;

    let mut rdr = csv::ReaderBuilder::new()
        .has_headers(true)
        .from_reader(contents.as_bytes());

    let rows: Vec<CSVRow> = rdr
        .deserialize()
        .filter_map(|x| x.expect("Deserializing row"))
        .collect();

    // Ensure there are a non-zero number of rows
    if rows.is_empty() {
        return Err(eyre!("Empty file"));
    }

    // Iterate over the file and use a simple state machine
    // to assemble packets.
    let mut packets = Vec::<Packet>::new();
    let mut state = PacketStateMachine::SyncWaiting;

    for row in rows {
        // Perform the state transition
        state = state.transition(row);

        // Find whether we're at a final state
        // which will also give us a packet
        let (new_state, packet) = state.read_packet(args.skip_checksums);
        if let Some(packet) = packet {
            trace!("Got packet: {:?}", &packet);
            packets.push(packet)
        }
        state = new_state;
    }

    let mut bad_checksums = 0;
    let packet_count = packets.len();

    // Check whether we can create the output filename
    if let Some(output_path) = args.output_file {
        let file = File::create(&output_path)?;
        let mut wtr = csv::WriterBuilder::new()
            .has_headers(false)
            .flexible(true)
            .from_writer(file);

        for packet in packets.iter() {
            if args.skip_checksums == false {
                if !packet.valid_checksum {
                    bad_checksums += 1;
                }
            }
            wtr.serialize(packet)?;
        }
        wtr.flush()?;
    }

    println!(
        "Finished processing packets. {} of {} had a bad checksum.",
        bad_checksums, packet_count
    );

    Ok(())
}
