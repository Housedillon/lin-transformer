# LIN Transformer

This program takes the LIN analyzer output from Saleae Logic, and converts it from LIN events to LIN packets.

## Usage

```
Saleae logic LIN analyzer CSV output transformer

Usage: lin-transformer [OPTIONS] --input-file <INPUT_FILE>

Options:
  -i, --input-file <INPUT_FILE>    CSV input from the Saleae LIN analyzer output
  -o, --output-file <OUTPUT_FILE>  CSV output filename
  -s, --skip-checksums             Don't process checksums
  -h, --help                       Print help
  -V, --version                    Print version
  ```

  ## Examples transformation:

  The original contents of the Saleae analyzer is the following:

| name | type             | start_time | duration | protected_id | data | index | checksum |
|------|------------------|------------|----------|--------------|------|-------|----------|
| LIN  | header_break     | 2.758378   | 0.00202  |              |      |       |          |
| LIN  | header_sync      | 2.761375   | 0.00048  |              |      |       |          |
| LIN  | header_pid       | 2.761895   | 0.00048  | 4            |      |       |          |
| LIN  | data             | 2.762715   | 0.00048  |              | 1    | 0     |          |
| LIN  | data             | 2.763245   | 0.00048  |              | 3    | 1     |          |
| LIN  | data_or_checksum | 2.763775   | 0.00048  |              | 251  | 2     | 251      |
| LIN  | header_break     | 2.765367   | 0.00199  |              |      |       |          |
| ...  | ...              | ...        | ...      | ...          | ...  | ...   | ...      |
| LIN  | header_break     | 34.92632   | 0.00199  |              |      |       |          |
| LIN  | header_sync      | 34.92934   | 0.00048  |              |      |       |          |
| LIN  | header_pid       | 34.92986   | 0.00048  | 5            |      |       |          |
| LIN  | data             | 34.93038   | 0.00048  |              | 0    | 0     |          |
| LIN  | data             | 34.93089   | 0.00048  |              | 0    | 1     |          |
| LIN  | data             | 34.93141   | 0.00048  |              | 0    | 2     |          |
| LIN  | data             | 34.93193   | 0.00048  |              | 0    | 3     |          |
| LIN  | data_or_checksum | 34.93245   | 0.00048  |              | 255  | 4     | 255      |
| LIN  | header_break     | 34.93431   | 0.00198  |              |      |       |          |

These rows would produce the following:

|  Time  |  Duration | P. Valid? | C. Valid? | checksum | pid | d0  | d1  | d2  | d3  |
|--------|-----------|-----------|-----------|----------|-----|-----|-----|-----|-----|
| 2.7613 | 0.0044256 | true      | true      | 251      | 4   | 1   | 3   |     |     |
| ...    | ...       | ...       | ...       | ...      | ... | ... | ... | ... | ... |
| 34.929 | 0.0053945 | true      | true      | 255      | 5   | 0   | 0   | 0   |0    |

## Sample files

There are sample files included in this repo.  The Full.sal file is a saleae logic capture file that you can load into the application, even if you don't have a logic.  And the Full.csv file is the LIN analyzer output.